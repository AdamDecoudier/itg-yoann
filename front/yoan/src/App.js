import React, {Component} from 'react';
import './App.css';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pieces: [],
            clients: [],
            activeClient: "",
        };
        this.CreateSelectItems = this.CreateSelectItems.bind(this);
        this.HandleClientChange = this.HandleClientChange.bind(this);
    }

    componentDidMount() {
        fetch('http://localhost:8080/clients')
            .then(res => res.json())
            .then((data) => {
                this.setState({clients: data});
            })
            .catch(console.log);
        fetch('http://localhost:8080/pieces')
            .then(res => res.json())
            .then((data) => {
                this.setState({pieces: data});
            })
            .catch(console.log);
    }

    CreateSelectItems() {
        let items = [<option key="none" value="none">-----</option>];
        this.state.clients.forEach(c => {
            items.push(<option key={c} value={c}>{c}</option>);
        });
        return items;
    }

    LoadPieces() {
        var link = "http://localhost:8080/pieces";
        if (this.state.activeClient !== "none") {
            link += "/" + this.state.activeClient
        }
        fetch(link)
            .then(res => res.json())
            .then((data) => {
                this.setState({pieces: data});
            })
            .catch(console.log);
    }

    HandleClientChange(event) {
        this.setState({activeClient: event.target.value}, this.LoadPieces);
    }

    render() {
        return (
            <div>
                <div>
                    <p>Consultation des prix des produits</p>
                    <label htmlFor="client">Sélectionnez un client:</label>
                    <select id="client" onChange={this.HandleClientChange} value={this.state.activeClient}>
                        {this.CreateSelectItems()}
                    </select>
                </div>
                <div>
                    <table>
                        <thead>
                        <tr>
                            <td>Nom</td>
                            <td>Référence</td>
                            <td>Prix</td>
                        </tr>
                        </thead>
                        <tbody>
                        {(this.state.pieces.length > 0) ? this.state.pieces.map((piece, index) => {
                            return (
                                <tr key={index}>
                                    <td>{piece.Nom}</td>
                                    <td>{piece.Reference}</td>
                                    <td>{piece.Prix.toFixed(2)}</td>
                                </tr>
                            )
                        }) : <tr>
                            <td colSpan="5">Loading...</td>
                        </tr>}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default App;
