package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"mspr-yoann/src/config"
	"mspr-yoann/src/database"
	"mspr-yoann/src/http"
)

var serverPort = config.ServerPort

func main() {
	start()
	fmt.Println("Some random message to start")
}

func start() {

	database.Init()
	defer database.Close()

	fmt.Println("####### STARTING server #######")
	defer fmt.Println("####### server STOPPED #######")

	server := http.NewServer(serverPort)
	server.Start()
	defer server.Stop()

	println("Server started pid:", os.Getpid())
	fmt.Println("####### Server STARTED #######")

	waitForStop()
	fmt.Println("Shutting down")
}

func waitForStop() {
	c := make(chan os.Signal, 1)
	defer close(c)
	signal.Notify(c, os.Interrupt, syscall.SIGUSR1)
	defer signal.Stop(c)
	for s := range c {
		fmt.Println("Signal", s.String(), "received")
		switch s {
		case syscall.SIGUSR1:
			state()
		default:
			return
		}
	}
}

func state() {
	s := fmt.Sprintf("[STATE] Configuration used:\n")
	flag.VisitAll(func(f *flag.Flag) {
		if f.Name == "version" { // not a configuration flag
			return
		}
		s += fmt.Sprintf("\t%s = %s\n", f.Name, f.Value)
	})
	print(s)
	fmt.Print(s)
}
