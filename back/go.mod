module mspr-yoann

go 1.14

require (
	github.com/dimfeld/httptreemux v5.0.1+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/rs/cors v1.7.0
)
