package http

import (
	"fmt"
	nethttp "net/http"

	"mspr-yoann/src/services"
)

func getAllClientsHandler(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
	clients, err := services.FindAllClients()
	if err != nil {
		replyEntityError(rw, err)
		return
	}
	replyJSON(rw, nethttp.StatusOK, clients)
}

func addClientHandler(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
	fmt.Println("Testing create client")
	replyJSON(rw, nethttp.StatusOK, nil)
}

func deleteClientHandler(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
	fmt.Println("Testing delete client ")
	replyJSON(rw, nethttp.StatusOK, nil)
}
