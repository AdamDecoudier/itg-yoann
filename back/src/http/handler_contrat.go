package http

import (
	"fmt"
	nethttp "net/http"
)

func getAllContratsHandler(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
	fmt.Println("Testing get all contrats")
	replyJSON(rw, nethttp.StatusOK, nil)
}

func addContratHandler(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
	fmt.Println("Testing create contrat")
	replyJSON(rw, nethttp.StatusOK, nil)
}

func deleteContratHandler(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
	fmt.Println("Testing delete contrat")
	replyJSON(rw, nethttp.StatusOK, nil)
}
