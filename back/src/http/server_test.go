package http

import (
	nethttp "net/http"
	"testing"
	"time"
)

func TestStart(t *testing.T) {
	server := NewServer(8081)
	defer server.Stop()
	server.Start()
	time.Sleep(100 * time.Millisecond)
	_, err := nethttp.Get("http://localhost:8081/")
	if err != nil {
		t.Error("Message server did not start: error is ", err)
	}
	t.Log("Successfully connected to message server")
}

func TestStop(t *testing.T) {
	server := NewServer(8082)
	server.Start()
	server.Stop()
	time.Sleep(100 * time.Millisecond)
	_, err := nethttp.Get("http://localhost:8081/")
	if err == nil {
		t.Error("Message server did not stop")
	}
	t.Log("Server successfully stopped")
}
