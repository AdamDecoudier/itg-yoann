package http

import (
	"encoding/json"
	nethttp "net/http"
)

type testData struct {
	Message string
}

func homeHandler(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
	data := testData{Message: "Hello World !"}
	if err := json.NewEncoder(rw).Encode(data); err != nil {
		panic(err)
	}
}
