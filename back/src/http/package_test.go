package http

import (
	"os"
	"testing"

	"mspr-yoann/src/config"
)

func TestMain(m *testing.M) {

	os.Exit(func() int {
		port := config.ServerPort
		server := NewServer(port)
		server.Start()
		defer server.Stop()
		return m.Run()
	}())
}

func assertErr(t *testing.T, err error, cause string) {
	t.Helper()
	if err != nil {
		t.Fatalf("Test failed in %s, error is: %s", cause, err)
	}
}
