package http

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	nethttp "net/http"

	"github.com/dimfeld/httptreemux"
)

func replyJSON(rw nethttp.ResponseWriter, status int, obj interface{}) {
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(status)
	if err := json.NewEncoder(rw).Encode(obj); err != nil {
		replyErrorf(rw, nethttp.StatusInternalServerError, "unable to JSON encode %v: %s", obj, err)
		return
	}
}

func replyError(rw nethttp.ResponseWriter, status int, error string, err error) {
	logline := fmt.Sprintf("Replying error '%d : %s'", status, error)
	if err != nil { // underlying technical error
		logline += fmt.Sprintf(", reason was %s", err)
	}
	fmt.Println(logline)
	nethttp.Error(rw, error, status)
}

func replyErrorf(rw nethttp.ResponseWriter, status int, format string, args ...interface{}) {
	replyError(rw, status, fmt.Sprintf(format, args...), nil)
}

func replyErrorln(rw nethttp.ResponseWriter, status int, message ...interface{}) {
	replyError(rw, status, fmt.Sprintln(message...), nil)
}

func replyEntityError(rw nethttp.ResponseWriter, err error) {
	switch {
	case errors.Is(err, sql.ErrNoRows):
		replyError(rw, nethttp.StatusNotFound, "Not Found", err)
	default:
		replyError(rw, nethttp.StatusInternalServerError, "An unexpected error has occured", err)
	}
}

func CORSFilter(h httptreemux.HandlerFunc) httptreemux.HandlerFunc {
	return func(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
		header := rw.Header()
		header.Set("Access-Control-Allow-Origin", "*")
		header.Set("Access-Control-Max-Age", "3600")
		header.Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
		header.Set("Access-Control-Allow-Methods", "POST, PUT, DELETE, OPTIONS, GET")
		h(rw, req, ps)
	}
}
