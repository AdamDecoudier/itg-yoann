package http

import (
	"fmt"
	nethttp "net/http"

	"mspr-yoann/src/services"
)

func getAllPiecesHandler(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
	pieces, err := services.FindAllPieces()
	if err != nil {
		replyEntityError(rw, err)
		return
	}
	replyJSON(rw, nethttp.StatusOK, pieces)
}

func addPieceHandler(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
	fmt.Println("Testing add piece")
	replyJSON(rw, nethttp.StatusOK, nil)
}

func deletePieceHandler(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
	fmt.Println("Testing delete piece")
	replyJSON(rw, nethttp.StatusOK, nil)
}

func getPiecesByClientHandler(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
	client := ps["client"]
	pieces, err := services.FindPiecesByClient(client)
	if err != nil {
		replyEntityError(rw, err)
		return
	}
	replyJSON(rw, nethttp.StatusOK, pieces)
}
