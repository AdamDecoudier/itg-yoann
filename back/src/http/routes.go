package http

import (
	"github.com/dimfeld/httptreemux"
)

type Route struct {
	Method  string
	Path    string
	Handler httptreemux.HandlerFunc
}

var routes = []Route{
	// Routes de base
	{"OPTIONS", "/", optionsHandler("GET, POST, PUT, DELETE")},
	{"GET", "/", homeHandler},

	// Routes Client
	{"GET", "/clients", getAllClientsHandler},
	{"PUT", "/clients", addClientHandler},
	{"DELETE", "/clients", deleteClientHandler},

	// Routes Contrat
	{"GET", "/contrats", getAllContratsHandler},
	{"PUT", "/contrats", addContratHandler},
	{"DELETE", "/contrats", deleteContratHandler},

	// Routes Piece
	{"GET", "/pieces", getAllPiecesHandler},
	{"GET", "/pieces/:client", getPiecesByClientHandler},
	{"PUT", "/pieces", addPieceHandler},
	{"DELETE", "/pieces", deletePieceHandler},
}
