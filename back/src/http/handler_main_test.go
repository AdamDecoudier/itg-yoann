package http

import (
	"fmt"
	nethttp "net/http"
	"strconv"
	"testing"

	"mspr-yoann/src/config"
)

func TestCanSayHello(t *testing.T) {
	resp, err := nethttp.Get(fmt.Sprintf("http://localhost:%d", config.ServerPort))
	assertErr(t, err, "Get")
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		t.Fatal("StatusCode should be 200 but was ", strconv.Itoa(resp.StatusCode))
	}
}

func TestReplyNotFound(t *testing.T) {
	resp, err := nethttp.Get(fmt.Sprintf("http://localhost:%d/somerandomthing", config.ServerPort))
	assertErr(t, err, "Get")
	defer resp.Body.Close()

	if resp.StatusCode != 404 {
		t.Fatal("StatusCode should be 404 but was ", strconv.Itoa(resp.StatusCode))
	}
}

func TestMethodNotAllowed(t *testing.T) {
	resp, err := nethttp.Post(fmt.Sprintf("http://localhost:%d", config.ServerPort), "application/json", nil)
	assertErr(t, err, "Post")
	defer resp.Body.Close()

	if resp.StatusCode != 405 {
		t.Fatal("StatusCode should be 404 but was ", strconv.Itoa(resp.StatusCode))
	}
}
