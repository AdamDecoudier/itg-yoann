package http

import (
	"context"
	"fmt"
	"log"
	nethttp "net/http"
	"time"

	"github.com/dimfeld/httptreemux"
	"github.com/rs/cors"
)

var serverShutdownTimeout = 30 * time.Second

type Server struct {
	*nethttp.Server
}

func NewServer(port int) *Server {
	router := httptreemux.New()
	handler := cors.Default().Handler(router)
	for _, route := range routes {
		router.Handle(route.Method, route.Path, route.Handler)
	}
	return &Server{
		Server: &nethttp.Server{
			Handler: handler,
			Addr:    fmt.Sprintf(":%d", port),
		},
	}
}

func (s *Server) Start() {
	go func() {
		if err := s.ListenAndServe(); err != nethttp.ErrServerClosed {
			log.Fatal("Error while serving http:", err)
		}
	}()
	fmt.Println("HTTP server is listening to", s.Addr)
}

func (s *Server) Stop() {
	ctx, cancel := context.WithTimeout(context.Background(), serverShutdownTimeout)
	defer cancel()

	fmt.Printf("HTTP server is gracefully shutting down, waiting at most %s", serverShutdownTimeout)
	if err := s.Shutdown(ctx); err != nil {
		fmt.Printf("Cannot shutdown gracefully : %s", err)
	}
	fmt.Println("HTTP server stopped")
}

func optionsHandler(supportedMethods string) httptreemux.HandlerFunc {
	return func(rw nethttp.ResponseWriter, req *nethttp.Request, ps map[string]string) {
		rw.Header().Set("Allow-Methods", supportedMethods)
		rw.WriteHeader(nethttp.StatusOK)
	}
}
