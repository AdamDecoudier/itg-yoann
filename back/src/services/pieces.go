package services

import (
	"mspr-yoann/src/database"
	. "mspr-yoann/src/entities"
)

func CreatePiece(nom string, prix float64) error {
	return nil
}

func DeletePieceByID(id int) error {
	return nil
}

func DeletePieceByName(nom string) error {
	return nil
}

func FindAllPieces() ([]Piece, error) {
	return database.GetAllPieces()
}

func FindPiecesByClient(client string) ([]Piece, error) {
	return database.GetPiecesByClient(client)
}
