package services

import "mspr-yoann/src/database"

func FindAllClients() ([]string, error) {
	return database.GetAllClientsName()
}
