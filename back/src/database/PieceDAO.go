package database

import (
	"fmt"

	. "mspr-yoann/src/entities"
)

func CreatePiece(nom string, prix float64) error {
	return nil
}

func DeletePieceByID(id int) error {
	return nil
}

func DeletePieceByName(id int) error {
	return nil
}

func GetAllPieces() ([]Piece, error) {
	var pieces []Piece
	results, err := Database.Query("SELECT nom, reference, prix FROM pieces")
	if err != nil {
		err = fmt.Errorf("could not find all pieces : %w", err)
		return nil, err
	}

	for results.Next() {
		var piece Piece
		err = results.Scan(&piece.Nom, &piece.Reference, &piece.Prix)
		if err != nil {
			err = fmt.Errorf("could not parse all pieces : %w", err)
			return nil, err
		}
		pieces = append(pieces, piece)
	}
	return pieces, nil
}

func GetPiecesByClient(nom string) ([]Piece, error) {
	pieces, err := GetAllPieces()
	if err != nil {
		return nil, err
	}
	client, err := GetClientByName(nom)
	if err != nil {
		return nil, err
	}
	for i, piece := range pieces {
		pieces[i].Prix = piece.Prix * (1 + (float64(client.Contrat.Taxe) / 100))
	}
	return pieces, nil
}
