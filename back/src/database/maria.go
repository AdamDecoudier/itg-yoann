package database

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"

	"mspr-yoann/src/config"
)

var (
	Database *sql.DB
)

func Init() {
	fmt.Println("####### CONNECTING to database #######")
	db, err := sql.Open("mysql", config.DatabaseURI)
	if err != nil {
		panic(err.Error())
	}
	Database = db
	fmt.Println("####### CONNECTED to database #######")
}

func Close() {
	fmt.Println("####### DISCONNECTING from database #######")
	Database.Close()
	fmt.Println("####### DISCONNECTED from database #######")
}
