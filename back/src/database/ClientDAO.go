package database

import (
	"fmt"

	. "mspr-yoann/src/entities"
)

func GetAllClientsName() ([]string, error) {
	var clients []string
	results, err := Database.Query("SELECT nom FROM clients")
	if err != nil {
		err = fmt.Errorf("could not find all clients name : %w", err)
		return nil, err
	}

	for results.Next() {
		var client string
		err = results.Scan(&client)
		if err != nil {
			err = fmt.Errorf("could not parse all pieces : %w", err)
			return nil, err
		}
		clients = append(clients, client)
	}
	return clients, nil
}

func GetClientByName(nom string) (Client, error) {
	var client Client
	var contrat Contrat
	err := Database.QueryRow("SELECT taxe, numero "+
		"FROM clients "+
		"INNER JOIN contrats "+
		"ON clients.id_contrat = contrats.id "+
		"WHERE nom = ?", nom).Scan(&contrat.Taxe, &contrat.Numero)
	if err != nil {
		return client, fmt.Errorf("could not find contract details for %s : %w", nom, err) // proper error handling instead of panic in your app
	}
	client = Client{nom, contrat}
	return client, nil
}
