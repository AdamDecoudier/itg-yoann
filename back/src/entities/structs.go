package entities

type Client struct {
	Nom     string
	Contrat Contrat
}

type Contrat struct {
	Numero int
	Taxe   int
}

type Piece struct {
	Nom       string
	Reference string
	Prix      float64
}
